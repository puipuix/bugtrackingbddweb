# Procédure de lancement
***Ce fichier a été écrit au format MarkDown, certains caractères doivent être ignorés si ce fichier est ouvert dans un éditeur de texte.***
## Pour lancer le Back-End
- Déplacez vous dans le dosser bugtrackingBDDWEB:
*cd bugtrackingBDDWEB* 

- Lancez le server avec la commande :
*mvnw spring-boot:run*

## Pour lancer le Front-End
- Déplacez vous dans le dosser bugtrackingFRONT :
*cd bugtrackingFRONT*

- Il faut au préalable installer les outils de compilation Angular car ils ne sont pas présent sur ce Git :
*npm install*

- Une fois le téléchargement fini il est possible de lancer le server avec la commande :
*ng serve --open*
