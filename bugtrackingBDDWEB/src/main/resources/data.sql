INSERT INTO MY_EMPLOYEE (id,name,avatar) VALUES
  (1, 'Employee1','https://cdn.pixabay.com/photo/2020/03/23/08/45/cat-4959941_1280.jpg'),
  (2, 'Employee2','https://cdn.pixabay.com/photo/2020/11/12/13/44/labrador-retriever-5735582__340.jpg'),
  (3, 'Employee3','https://upload.wikimedia.org/wikipedia/commons/6/6c/SpaceX_SN5_Starship_150m_Hop_%26_Powerslide.jpg'),
  (4, 'Employee4','https://cdn.pixabay.com/photo/2019/02/21/22/27/caracal-4012455__340.jpg');
  
INSERT INTO MY_BUG (id, title, description, state, priority, creation) VALUES
  (1, 'Bug1', 'description', 0, 0, CURRENT_TIMESTAMP),
  (2, 'Bug2', 'description', 1, 0, CURRENT_TIMESTAMP);
  
INSERT INTO MY_BUG (id, title, description, employee_id, state, priority, creation) VALUES
  (3, 'Bug3', 'description', 1, 2, 0, CURRENT_TIMESTAMP);

INSERT INTO MY_COMMENTARY (id, text, employee_id, bug_id) VALUES
  (1, 'com1 de emp1', 1, 3),
  (2, 'com2 de emp1', 1, 1),
  (3, 'com3 de emp2', 2, 3);