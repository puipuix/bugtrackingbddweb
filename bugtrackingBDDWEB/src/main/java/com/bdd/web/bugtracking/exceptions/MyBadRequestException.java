package com.bdd.web.bugtracking.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MyBadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6415246148313090319L;

	public MyBadRequestException() {
		super();
	}

	public MyBadRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MyBadRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public MyBadRequestException(String message) {
		super(message);
	}

	public MyBadRequestException(Throwable cause) {
		super(cause);
	}
}
