package com.bdd.web.bugtracking.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.bdd.web.bugtracking.entities.MyBug;
import com.bdd.web.bugtracking.entities.MyBugPriority;
import com.bdd.web.bugtracking.entities.MyBugState;

public interface MyBugRepository extends JpaRepository<MyBug, Integer> {

	public List<MyBug> findByPriority(@Param("priority") MyBugPriority priority);
	
	public List<MyBug> findByState(@Param("state") MyBugState state);
	
	public List<MyBug> findByTitleContainingIgnoreCase(@Param("title") String title);
}
