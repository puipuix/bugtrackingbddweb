package com.bdd.web.bugtracking.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bdd.web.bugtracking.entities.MyEmployee;

public interface MyEmployeeRepository extends JpaRepository<MyEmployee, Integer> {
	
	@Query("SELECT s FROM MyEmployee s WHERE s.name = :name")
	List<MyEmployee> getEmployeeByName(@Param("name") String name);
	


}
