package com.bdd.web.bugtracking.entities;

import com.bdd.web.bugtracking.repositories.MyBugRepository;
import com.bdd.web.bugtracking.repositories.MyEmployeeRepository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MyCreateCommentary {

	private int employee;
	private int bug;
	private String text;
	
	public MyEmployee getEmployee(MyEmployeeRepository r) {
		return r.findById(employee).orElse(null);
	}
	
	public MyBug getBug(MyBugRepository r) {
		return r.findById(bug).orElse(null);
	}
}
