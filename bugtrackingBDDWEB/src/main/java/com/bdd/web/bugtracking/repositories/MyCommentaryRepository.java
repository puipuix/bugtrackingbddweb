package com.bdd.web.bugtracking.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdd.web.bugtracking.entities.MyCommentary;

public interface MyCommentaryRepository extends JpaRepository<MyCommentary, Integer> {
	
}
