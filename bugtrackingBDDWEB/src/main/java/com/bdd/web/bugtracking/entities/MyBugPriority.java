package com.bdd.web.bugtracking.entities;

public enum MyBugPriority {
	low, normal, high;
}
