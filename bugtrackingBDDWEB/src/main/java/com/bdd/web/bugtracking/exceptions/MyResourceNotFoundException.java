package com.bdd.web.bugtracking.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MyResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -207735330643972406L;

	public MyResourceNotFoundException() {
		super();
	}

	public MyResourceNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MyResourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public MyResourceNotFoundException(String message) {
		super(message);
	}

	public MyResourceNotFoundException(Throwable cause) {
		super(cause);
	}

	
}
