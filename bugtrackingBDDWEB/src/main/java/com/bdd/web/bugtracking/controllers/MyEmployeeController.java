package com.bdd.web.bugtracking.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bdd.web.bugtracking.entities.CreateEmployee;
import com.bdd.web.bugtracking.entities.MyEmployee;
import com.bdd.web.bugtracking.repositories.MyBugRepository;
import com.bdd.web.bugtracking.repositories.MyCommentaryRepository;
import com.bdd.web.bugtracking.repositories.MyEmployeeRepository;

@RestController
public class MyEmployeeController {

	@Autowired
	MyBugRepository bugRepository;
	
	@Autowired
	MyEmployeeRepository employeesRepository;

	@Autowired
	MyCommentaryRepository commentaryRepository;
	
	@GetMapping("Employee")
	public List<MyEmployee> getAllEmployee() {

		return employeesRepository.findAll();
	}

	@GetMapping("EmployeeById")
	public MyEmployee getEmployeeById(@RequestParam(value = "id", defaultValue = "1") Integer id) {
		return employeesRepository.findById(id).orElse(null);
	}

	@GetMapping("EmployeeByName")
	public List<MyEmployee> getEmployeeByName(@RequestParam(value = "name", defaultValue = "") String name) {

		return employeesRepository.getEmployeeByName(name);
	}

	/*@PostMapping("Employees")
	public MyEmployee createEmployee(@Validated @RequestBody CreateEmployee myEmployee) {
		return null;
	}*/
	
	@PostMapping("CreateEmployee")
	public MyEmployee createEmployee(@Validated @RequestBody CreateEmployee
	myEmployee) {
	 return employeesRepository.save(MyEmployee
	 .builder().name(myEmployee.getName()).avatar(myEmployee.getAvatar())
	 .build());
	}
	
	 @DeleteMapping("Employee/{id}")
	    public ResponseEntity<?> deleteEmployee(@PathVariable("id") Integer id) throws Exception {
	        if(!employeesRepository.existsById(id)) {
	            throw new Exception("Employee not found with id " + id);
	        }

	        return employeesRepository.findById(id)
	                .map(employee -> {
	                	employee.getBugs().forEach(bug -> {
	                		bug.setEmployee(null);
	                		bugRepository.save(bug);
	                	});
	                	employee.getCommentaries().forEach(com -> {
							com.setEmployee(null);
							commentaryRepository.save(com);
						});
	                    employeesRepository.delete(employee);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new Exception("employee not found with id " + id));

	    }

}
