package com.bdd.web.bugtracking.entities;

public enum MyBugState {
	todo, wip, done;
}
