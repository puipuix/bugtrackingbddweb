package com.bdd.web.bugtracking.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bdd.web.bugtracking.entities.MyBug;
import com.bdd.web.bugtracking.entities.MyBugPriority;
import com.bdd.web.bugtracking.entities.MyBugState;
import com.bdd.web.bugtracking.entities.MyCreateBug;
import com.bdd.web.bugtracking.exceptions.MyResourceNotFoundException;
import com.bdd.web.bugtracking.repositories.MyBugRepository;
import com.bdd.web.bugtracking.repositories.MyCommentaryRepository;
import com.bdd.web.bugtracking.repositories.MyEmployeeRepository;

import io.swagger.annotations.ApiParam;

@RestController
public class MyBugController {

	@Autowired
	MyBugRepository bugRepository;

	@Autowired
	MyCommentaryRepository commentaryRepository;

	@Autowired
	MyEmployeeRepository employeeRepository;

	@GetMapping("bugs/{id}")
	public MyBug findById(@PathVariable("id") Integer id) {
		return bugRepository.findById(id)
				.orElse(null);
	}

	@GetMapping("bugs")
	public List<MyBug> findAll() {
		
		return bugRepository.findAll();
	}

	@GetMapping("bugs/priority/{priority}")
	public List<MyBug> findByPriority(@PathVariable("priority") MyBugPriority priority) {
		return bugRepository.findByPriority(priority);
	}

	@GetMapping("bugs/state/{state}")
	public List<MyBug> findByState(@PathVariable("state") MyBugState state) {
		return bugRepository.findByState(state);
	}

	@GetMapping("bugs/title/{title}")
	public List<MyBug> findByTitle(@PathVariable("title") String title) {
		return bugRepository.findByTitleContainingIgnoreCase(title);
	}

	@PostMapping("bugs")
	public MyBug createBug(@Validated @RequestBody MyCreateBug bug) {
		return bugRepository.save(MyBug.builder()
				.title(bug.getTitle())
				.creation(bug.getCreation())
				.description(bug.getDescription())
				.priority(bug.getPriority())
				.employee(bug.getEmployee(employeeRepository))
				.state(bug.getState())
				.build());
	}

	@DeleteMapping("bugs/{id}")
	public ResponseEntity<?> deleteBug(@PathVariable("id") Integer id) {
		return bugRepository.findById(id)
				.map(bug -> {
					bug.getCommentaries().forEach(com -> {
						commentaryRepository.delete(com);
					});
					bugRepository.delete(bug);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new MyResourceNotFoundException("Bug not found with id " + id));

	}

	@PutMapping("bugs/{id}/employee/{employee}")
	public ResponseEntity<?> updateBugEmployee(@PathVariable("id") Integer id,
			@PathVariable("employee") @ApiParam("The id of the employee") Integer employee) {
		return bugRepository.findById(id)
				.map(bug -> {
					if (employee == 0) {
						bug.setEmployee(null);
						bugRepository.save(bug);
						return ResponseEntity.ok().build();
					} else {
						return employeeRepository.findById(employee)
								.map(e -> {
									bug.setEmployee(e);
									bugRepository.save(bug);
									return ResponseEntity.ok().build();
								})
								.orElseThrow(() -> new MyResourceNotFoundException("Employee not found with id " + id));
					}
				}).orElseThrow(() -> new MyResourceNotFoundException("Bug not found with id " + id));

	}

	@PutMapping("bugs/{id}/priority/{priority}")
	public ResponseEntity<?> updateBugPriority(@PathVariable("id") Integer id,
			@PathVariable("priority") MyBugPriority priority) {
		return bugRepository.findById(id)
				.map(bug -> {
					bug.setPriority(priority);
					bugRepository.save(bug);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new MyResourceNotFoundException("Bug not found with id " + id));

	}

	@PutMapping("bugs/{id}/state/{state}")
	public ResponseEntity<?> updateBugState(@PathVariable("id") Integer id, @PathVariable("state") MyBugState state) {
		return bugRepository.findById(id)
				.map(bug -> {
					bug.setState(state);
					bugRepository.save(bug);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new MyResourceNotFoundException("Bug not found with id " + id));

	}
}
