package com.bdd.web.bugtracking.entities;

import java.util.Date;

import com.bdd.web.bugtracking.repositories.MyEmployeeRepository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MyCreateBug {

	private String title;
	private String description;
	private MyBugPriority priority;
	private MyBugState state;
	private Date creation;
	private int employee;
	
	public MyEmployee getEmployee(MyEmployeeRepository r) {
		return r.findById(employee).orElse(null);
	}
}
