package com.bdd.web.bugtracking.entities;

import com.bdd.web.bugtracking.controllers.MyEmployeeController;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateEmployee {
    private String name;
    private String avatar; 
}
