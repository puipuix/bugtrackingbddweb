package com.bdd.web.bugtracking.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class MyEmployee {

	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String avatar;
	
	@OneToMany(mappedBy = "employee")
	@JsonIgnoreProperties({ "commentaries", "employee" })
	private List<MyBug> bugs;
	@OneToMany(mappedBy = "employee")
	@JsonIgnoreProperties({ "employee" })
	private List<MyCommentary> commentaries;
}
