package com.bdd.web.bugtracking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bdd.web.bugtracking.entities.MyCommentary;
import com.bdd.web.bugtracking.entities.MyCreateCommentary;
import com.bdd.web.bugtracking.exceptions.MyResourceNotFoundException;
import com.bdd.web.bugtracking.repositories.MyBugRepository;
import com.bdd.web.bugtracking.repositories.MyCommentaryRepository;
import com.bdd.web.bugtracking.repositories.MyEmployeeRepository;


@RestController
public class MyCommentaryController {

	@Autowired
	MyCommentaryRepository commentaryRepository;

	@Autowired
	MyBugRepository bugRepository;

	@Autowired
	MyEmployeeRepository employeeRepository;

	@PostMapping("commentaries")
	public MyCommentary createCommentary(@Validated @RequestBody MyCreateCommentary commentary) {
		return commentaryRepository.save(MyCommentary.builder()
				.employee(commentary.getEmployee(employeeRepository))
				.bug(commentary.getBug(bugRepository))
				.text(commentary.getText())
				.build());
	}

	@DeleteMapping("commentaries/{id}")
	public ResponseEntity<?> deleteCommentary(@PathVariable("id") Integer id) {
		if (!commentaryRepository.existsById(id)) {
			throw new MyResourceNotFoundException("Commentary not found with id " + id);
		}

		return commentaryRepository.findById(id)
				.map(commentary -> {
					commentaryRepository.delete(commentary);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new MyResourceNotFoundException("Commentary not found with id " + id));
	}
}
