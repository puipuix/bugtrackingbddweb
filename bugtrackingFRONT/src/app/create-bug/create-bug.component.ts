import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BugService } from '../bug.service';
import { EmployeeService } from '../employee.service';
import { Bug } from '../models/bug';
import { CreateBug } from '../models/createBug';
import { Employee } from '../models/employee';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-create-bug',
  templateUrl: './create-bug.component.html',
  styleUrls: ['./create-bug.component.css']
})
export class CreateBugComponent implements OnInit {
  createForm:FormGroup
  employees:Employee[] 
  
  @Output() createEvent=new EventEmitter<Bug>()
  constructor(public formBuilder:FormBuilder,
	private bugService:BugService,
	private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.createForm=this.formBuilder.group({
      title:'',
      description:'',
	  employee:'',
      bugpriority:''
    });
	this.employeeService.getEmployeeList().subscribe((
      employeeResponse=>{
        this.employees=employeeResponse
      }
    ))
  }

  onSubmit(bugData):void{
    this.createForm.reset();
    //prompt(new Date())
    const bug:CreateBug={
		title: bugData.title,
		description: bugData.description, 
		employee:bugData.employee,
		priority:bugData.bugpriority,
		state: "todo",
		creation: new Date()
    };

    this.bugService.createBug(bug).subscribe((bugResponse)=>{
      this.createEvent.emit(bugResponse);
    });
  }

}
