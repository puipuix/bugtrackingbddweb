import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {EmployeeService} from '../employee.service'
import { Employee } from '../models/employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
createForm:FormGroup

@Output() createEvent= new EventEmitter<Employee>();

  constructor(public formBuilder: FormBuilder,private employeeService : EmployeeService) { }

  ngOnInit(): void {
    this.createForm=this.formBuilder.group({
      name:'',
      avatar:''
    });
  }

  onSubmit(employeeData): void{
    this.createForm.reset();
  
    const employee:Employee={
      name:employeeData.name,
      avatar:employeeData.avatar,
      bugs:[]
    };
    this.employeeService.createEmployee(employee).subscribe((employeeResponse)=>{
      this.createEvent.emit(employeeResponse);
    });
  }

}
