import { Component, OnInit } from '@angular/core';
import { BugService } from '../bug.service';
import { Bug } from '../models/Bug';

@Component({
  selector: 'app-bug',
  templateUrl: './bug.component.html',
  styleUrls: ['./bug.component.css']
})
export class BugComponent implements OnInit {

  public bugsWip : Bug[]=[]
  public bugsTodo : Bug[]=[]
  public bugsDone : Bug[]=[]

  constructor(private bugService : BugService) { }

  ngOnInit(): void {
    
    this.bugService.getBugList().subscribe
    ((bugResponse=>{
     
      bugResponse.forEach(element => {
        if (element.state=="todo"){
          this.bugsTodo.push(element)
        }else if (element.state=="wip"){
          this.bugsWip.push(element)
        }else{
          this.bugsDone.push(element)
        }
      });

    }),(error=>{
      console.log(error.message);
    }));

  }


  addBug(newBug : Bug):void{
    if(newBug.state=="todo"){
      this.bugsTodo.push(newBug)
    }else if(newBug.state=="wip"){
      this.bugsWip.push(newBug)
    }else{
      this.bugsDone.push(newBug)
    }
  }

}
