import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { BugComponent } from './bug/bug.component';
import { DetailBugComponent } from './detail-bug/detail-bug.component';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';
import { EmployeeComponent } from './employee/employee.component';
import { SearchBugComponent } from './search-bug/search-bug.component';

const routes: Routes = [{ path: 'employee-component', component: EmployeeComponent },
{path :'detail-bug-component/:id',component:DetailBugComponent},
{path:'detail-employee-component',component:DetailEmployeeComponent},
{path:'bug-component',component: BugComponent},
{path:'search',component: SearchBugComponent}
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }