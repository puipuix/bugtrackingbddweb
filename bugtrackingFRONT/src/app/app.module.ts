import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BugComponent } from './bug/bug.component';
import { CreateBugComponent } from './create-bug/create-bug.component';
import { DetailBugComponent } from './detail-bug/detail-bug.component';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';
import { SearchBugComponent } from './search-bug/search-bug.component';



@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    CreateEmployeeComponent,
    BugComponent,
    CreateBugComponent,
    DetailBugComponent,
    DetailEmployeeComponent,
    SearchBugComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,AppRoutingModule,FormsModule,ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
