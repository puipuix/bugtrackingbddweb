import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BugService } from '../bug.service';
import { Bug } from '../models/Bug';

@Component({
  selector: 'app-search-bug',
  templateUrl: './search-bug.component.html',
  styleUrls: ['./search-bug.component.css']
})

export class SearchBugComponent implements OnInit {
	public bugs : Bug[]=[]
	searchForm:FormGroup
  constructor(public formBuilder:FormBuilder,private bugService:BugService) { }

  ngOnInit(): void {
	  this.searchForm=this.formBuilder.group({
      title:'',
    });
  }

	onSubmit(data):void{
		//this.searchForm.reset();
		this.bugService.getBugByTitle(data.title).subscribe((bugResponses)=>{
		  this.bugs = bugResponses;
		});
	}
}
