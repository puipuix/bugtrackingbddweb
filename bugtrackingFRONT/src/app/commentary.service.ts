import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Commentary } from './models/commentary';

@Injectable({
  providedIn: 'root'
})
export class CommentaryService {

  constructor(private http:HttpClient) { }

  public addCommentary(commentary:Commentary):Observable<Commentary>{
    return this.http.post<Commentary>(`${environment.url}/commentaries`,commentary);
  }
}
