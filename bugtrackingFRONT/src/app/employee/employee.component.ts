import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

import { Employee } from '../models/employee';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

public employees:Employee[]; 
public showSpinner: boolean

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.showSpinner=true;
    
    this.employeeService.getEmployeeList().subscribe
    ((employeeResponse=>{
     
      this.employees=employeeResponse;
      this.showSpinner=false;      
    }),(error=>{
      
    }));
  }

  addEmployee(newEmployee : Employee):void{
    this.employees.push(newEmployee);
  }

}
