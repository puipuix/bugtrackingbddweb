

export interface Commentary{
    id?: number
    text:string
    employee:string
    bug:string
}