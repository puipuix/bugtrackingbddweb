import { Commentary } from "./commentary";
import { Employee } from "./employee";

export interface Bug{
    id?: number
    title: string
    description: string 
    employee:Employee
    commentaries:Commentary[]
    priority:string
    state: string
    creation:Date
}