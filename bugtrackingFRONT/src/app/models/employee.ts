import { Bug } from "./bug";

export interface Employee{
    id?: number
    name: string
    avatar: string
    bugs: Bug[]
}