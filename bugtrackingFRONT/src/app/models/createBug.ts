export interface CreateBug{
    title: string
    description: string 
    employee:number
    priority:string
    state: string
    creation:Date
}