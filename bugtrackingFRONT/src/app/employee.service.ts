import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Employee } from './models/employee';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  public getEmployeeList(): Observable<Employee[]>{

   
    return this.http.get<Employee[]>(`${environment.url}/Employee`);
  }

  public getEmployeeById(id:string):Observable<Employee>{

    return this.http.get<Employee>(`${environment.url}/EmployeeById?id=`+id);
  }

  public createEmployee(employee:Employee):Observable<Employee>{
    return this.http.post<Employee>(`${environment.url}/CreateEmployee`,employee);
  }


} 
