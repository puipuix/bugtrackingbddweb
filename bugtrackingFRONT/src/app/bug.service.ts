import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bug } from './models/bug';
import { CreateBug } from './models/createBug';
import { environment } from '../environments/environment';
import { Commentary } from './models/commentary';

@Injectable({
  providedIn: 'root'
})
export class BugService {

  constructor(private http:HttpClient) { }

  public getBugList():Observable<Bug[]>
  {
    return this.http.get<Bug[]>(`${environment.url}/bugs`);
  }

  public createBug(bug:CreateBug):Observable<Bug>{
    return this.http.post<Bug>(`${environment.url}/bugs`,bug);
  }

  public getBugById(id:string):Observable<Bug>{
 
    return this.http.get<Bug>(`${environment.url}/bugs/`+id);

  }

  public getBugByTitle(title:string):Observable<Bug[]>{
	  return this.http.get<Bug[]>(`${environment.url}/bugs/title/`+title);
  }
  public changeState(id:string,state:string):Observable<Bug>{
     return this.http.put<Bug>(`${environment.url}/bugs/`+id+'/state/'+state,{})
  }

  public changePriority(id:string,priority:string):Observable<Bug>{
     return this.http.put<Bug>(`${environment.url}/bugs/`+id+'/priority/'+priority,{})
  }

	public deleteBug(id:string):Observable<{}>{
		return this.http.delete(`${environment.url}/bugs/`+id)
	}
}
