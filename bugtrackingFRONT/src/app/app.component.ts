import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bug Tracking';
  current_page = 'None';
  
	constructor(private router: Router) {
		this.router.events.subscribe(e => {
			if (e instanceof NavigationEnd){
				if (!(e.url == null)){
					this.current_page = e.url;
					//console.log(this.current_page);
				} else {
					//console.log("null or undefined");
				}
			}
		});
	}
}
