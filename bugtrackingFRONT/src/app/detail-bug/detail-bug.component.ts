import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BugService } from '../bug.service';
import { Bug } from '../models/bug';
import { Employee } from '../models/employee';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Commentary } from '../models/commentary';
import { CommentaryService } from '../commentary.service';
import { EmployeeService } from '../employee.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-detail-bug',
  templateUrl: './detail-bug.component.html',
  styleUrls: ['./detail-bug.component.css']
})
export class DetailBugComponent implements OnInit {
  createForm:FormGroup
  prioForm:FormGroup
  
  public listEmployee:Employee[]
  public bug:Bug;
  public employee:Employee
  modifPrio: boolean = false
	date:string
  id:string
  constructor(private router: Router, private route:ActivatedRoute,private employeeService:EmployeeService,private commentaryService:CommentaryService, private bugService:BugService,public formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      this.id=params['id'];
    });
    this.createForm=this.formBuilder.group({
      text:'',
      employee:'',
      bug:''
    });
    this.prioForm=this.formBuilder.group({
      priority:''
    });
    this.employeeService.getEmployeeList().subscribe((
      employeeResponse=>{
        this.listEmployee=employeeResponse
      }
    ))


    this.bugService.getBugById(this.id).subscribe
    ((bugResponse=>{

      this.bug=bugResponse;  
      this.employee= bugResponse.employee;
		this.date = formatDate(this.bug.creation,'longDate','en-US');
    }),(error=>{

    }));

    
  }

  onSubmit(commentaryData){
    
    this.createForm.reset();
    const commentary:Commentary={
      text:commentaryData.text,
      employee:commentaryData.employee,
      bug:this.bug.id+""
    };
    this.commentaryService.addCommentary(commentary).subscribe((commentaryResponse)=>{
      this.bug.commentaries.push(commentaryResponse)
    })
  }
  
  onSubmit2(data){
    
    this.prioForm.reset();
	this.bugService.changePriority(this.bug.id+"",data.priority).subscribe ((employeeResponse=>{
		this.bug.priority=data.priority
      }),(error=>{
      
      }));
	this.modifPrio = false;
  }

  onClick(){
    if(this.bug.state=="todo"){
      this.bugService.changeState(this.bug.id+"","wip").subscribe ((employeeResponse=>{
		this.bug.state="wip"
      }),(error=>{
      
      }));
      
    }else{
      this.bugService.changeState(this.bug.id+"","done").subscribe ((employeeResponse=>{
        this.bug.state="done"
      }),(error=>{
      
      }));
      
    }
	
	
  }
	onClick2(){
		this.modifPrio = true;
	}

	supprimer(){
		this.bugService.deleteBug(this.bug.id+"").subscribe ((employeeResponse=>{
        this.router.navigate(['/bug-component', { }]);
      }),(error=>{
		
      }));
	}
}
